import {useState, useEffect} from 'react';
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  Image,
} from 'react-native';
import React from 'react';
import {BASE_URL, TOKEN} from './url';

const AddData = ({navigation, route}) => {
  useEffect(() => {
    if (route.params) {
      const data = route.params;
      setNamaMobil(data.title);
      setTotalKM(data.totalKM);
      setHargaMobil(data.harga);
    }
  }, []);

  const [namaMobil, setNamaMobil] = useState('');
  const [totalKM, setTotalKM] = useState('');
  const [hargaMobil, setHargaMobil] = useState('');

  var dataMobil = route.params;

  const checkTextInput = () => {
    var submitAble = true;
    if (!namaMobil.trim()) {
      alert('Masukkan Nama Mobil');
      submitAble = false;
    }
    if (!totalKM.trim()) {
      alert('Masukkan Total KM');
      submitAble = false;
    }
    if (!hargaMobil.trim()) {
      alert('Masukkan Harga Mobil');
      submitAble = false;
    }
    if (hargaMobil < 100000000) {
      alert('Harga Mobil Harus Diatas Rp 100 Juta');
      submitAble = false;
    }
    return submitAble;
  };

  const postData = async () => {
    if (checkTextInput()) {
      const body = [
        {
          title: namaMobil,
          harga: hargaMobil,
          totalKM: totalKM,
          unitImage:
            'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRrhVioZcYZix5OUz8iGpzfkBJDzc7qPURKJQ&usqp=CAU',
        },
      ];
      try {
        const response = await fetch(`${BASE_URL}mobil`, {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
            Authorization: TOKEN,
          },
          body: JSON.stringify(body),
        });

        const result = await response.json();
        console.log('Success: ', result);
        alert('Data Berhasil Ditambahkan');
        navigation.goBack();
      } catch (error) {
        console.error('Error: ', error);
      }
    }
  };

  const putData = async () => {
    const body = [
      {
        _uuid: dataMobil._uuid,
        title: namaMobil,
        harga: hargaMobil,
        totalKM: totalKM,
        unitImage:
          'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRrhVioZcYZix5OUz8iGpzfkBJDzc7qPURKJQ&usqp=CAU',
      },
    ];
    try {
      const response = await fetch(`${BASE_URL}mobil`, {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json',
          Authorization: TOKEN,
        },
        body: JSON.stringify(body),
      });

      const result = await response.json();
      console.log('Success: ', result);
      alert('Data Berhasil Diubah');
      navigation.goBack();
    } catch (error) {
      console.error('Error: ', error);
    }
  };

  const deleteData = async () => {
    const body = [
      {
        _uuid: dataMobil._uuid,
      },
    ];
    try {
      const response = await fetch(`${BASE_URL}mobil`, {
        method: 'DELETE',
        headers: {
          'Content-Type': 'application/json',
          Authorization: TOKEN,
        },
        body: JSON.stringify(body),
      });

      const result = await response.json();
      alert('Data Berhasil Dihapus');
      navigation.goBack();
    } catch (error) {
      console.error('Error: ', error);
    }
  };

  return (
    <View style={{flex: 1, backgroundColor: '#fff'}}> 
      <View
        style={{
          width: '100%',
          flexDirection: 'row',
          alignItems: 'center',
        }}>
        <TouchableOpacity
          onPress={() => navigation.goBack()}
          style={{
            width: '10%',
            justifyContent: 'center',
            alignItems: 'center',
            paddingVertical: 10,
          }}>
          <Image
            style={{width: 24, height: 24}}
            source={require('../assets/icons/Close.png')}
          />
        </TouchableOpacity>
        <Text style={{fontSize: 16, fontWeight: 'bold', color: '#000'}}>
          {dataMobil ? 'Ubah Data' : 'Tambah Data'}
        </Text>
      </View>
      <View
        style={{
          width: '100%',
          padding: 15,
        }}>
        <View>
          <Text style={{fontSize: 16, color: '#000', fontWeight: '600'}}>
            Nama Mobil
          </Text>
          <TextInput
            value={namaMobil}
            onChangeText={text => setNamaMobil(text)}
            placeholder="Masukkan Nama Mobil"
            style={styles.txtInput}
          />
        </View>
        <View style={{marginTop: 20}}>
          <Text style={{fontSize: 16, color: '#000', fontWeight: '600'}}>
            Total Kilometer
          </Text>
          <TextInput
            value={totalKM}
            onChangeText={text => setTotalKM(text)}
            placeholder="contoh: 100 KM"
            style={styles.txtInput}
          />
        </View>
        <View style={{marginTop: 20}}>
          <Text style={{fontSize: 16, color: '#000', fontWeight: '600'}}>
            Harga Mobil
          </Text>
          <TextInput
            value={hargaMobil}
            onChangeText={text => setHargaMobil(text)}
            placeholder="Masukkan Harga Mobil"
            style={styles.txtInput}
            keyboardType="number-pad"
          />
        </View>
        <TouchableOpacity
          style={styles.btnAdd}
          onPress={() => (dataMobil ? putData() : postData())}>
          <Text style={{color: '#fff', fontWeight: '600'}}>
            {dataMobil ? 'Ubah Data' : 'Tambah Data'}
          </Text>
        </TouchableOpacity>
        {dataMobil ? (
          <TouchableOpacity onPress={()=>deleteData()} style={[styles.btnAdd, {backgroundColor: 'red'}]}>
            <Text style={{color: '#fff', fontWeight: '600'}}>Hapus Data</Text>
          </TouchableOpacity>
        ) : null}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  btnAdd: {
    marginTop: 20,
    width: '100%',
    paddingVertical: 10,
    borderRadius: 6,
    backgroundColor: '#689f38',
    justifyContent: 'center',
    alignItems: 'center',
  },
  txtInput: {
    marginTop: 10,
    width: '100%',
    borderRadius: 6,
    paddingHorizontal: 10,
    borderColor: '#dedede',
    borderWidth: 1,
  },
});

export default AddData;
